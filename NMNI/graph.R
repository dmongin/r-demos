setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
rm(list = ls())
gc()

library(data.table)
library(stringr)
library(ggplot2)
library(dplyr)
library(dbplyr)
library(RSQLite)
library(DBI)
library(lubridate)
library(ggfx)
library(ggimage)
library(patchwork)
library(ggtext)
library(waffle)

#### data management ####
# load db

db <- dbConnect(RSQLite::SQLite(), dbname = "NMNI.sqlite")
dbListTables(db)

article <- dbGetQuery(db, "SELECT * FROM article_info") %>% setDT()
authors <- dbGetQuery(db, "SELECT * FROM author_info") %>% setDT()
dates <- dbGetQuery(db, "SELECT * FROM date_info") %>% setDT()

# calculate rank
authors[,rank := .I - .I[1] + 1,by = doi]
authors[,N := .N,by = doi]
authors[rank == N,rank_c := "last"]
authors[rank == (N-1),rank_c := "last-1"]
authors[is.na(rank_c),rank_c := rank]

# subset raoult publis
raoult_publi <- authors[surnames == "Raoult",
                        .(doi ,rank_c )]


dates[,date := dmy(date)]
# calculated table of delay
delays <- dates[,.(revised = ifelse(any(type == "Revised"),1,0),
                   delay = date[type == "Accepted"] - date[type == "Received"],
                   published = if(any(type == "Accepted"))
                   {date[type == "Accepted"]} else
                   {date[type == "Available online"]}
),
                by = doi]

# create a variable indicating autorship of Raoult
delays[raoult_publi,raoult_last := ifelse(i.rank_c == "last","last","not_last"),on = "doi"]
delays[is.na(raoult_last),raoult_last := "not_raoult"]
delays[,raoult_last := factor(raoult_last,level = c("not_raoult","not_last","last"))]
# year variable
delays[,year := year(published)]

# table for the colors
colors = data.table(color = c("#d4d4d4","#0022ff","#0ee1e8"),
                    breaks = c("not_raoult","not_last","last"),
                    label = c("D. Raoult not author",
                              "D. Raoult not last author",
                              "D. Raoult last author"),
                    x = c(1,1,1),
                    y = c(1,2,3))


#### plot the delay between submission and acceptance ####

delay_plot <- ggplot(delays)+
  geom_jitter( aes(raoult_last,as.numeric(delay),group = raoult_last,
                   fill = as.factor(raoult_last)),
               alpha = .3,shape = 21,width = .2,size = 2)+
  geom_boxplot(aes(raoult_last,as.numeric(delay),group = raoult_last,
                   fill = as.factor(raoult_last)),
               position = position_nudge(x = .45, y = 0),
               width = .25,outlier.shape = NA,alpha = .6)+
  coord_flip()+
  scale_fill_manual(breaks = colors$breaks,
                    values = colors$color)+
  labs(y = "",
       subtitle = "Days between submission and acceptance in NMNI",
       title = "",
       x = "")+
  theme_minimal()+
  theme(axis.text.y.left = element_blank(),
        axis.title.y.left = element_blank(),
        panel.grid.major.y = element_blank(),
        axis.text = element_text(color = "black"),
        axis.title.x.bottom = element_text(margin = margin(t = 5,unit = "mm")),
        plot.subtitle = element_text(hjust = 0.5))+
  # scale_y_continuous(breaks = )+
  guides(fill = F)
delay_plot

#### plot the cmulated percentage of raoult's publications ####

delay_day <- delays[,.(N = .N),by = .(raoult_last,date = published)]
delay_day <- delay_day[CJ(raoult_last = unique(raoult_last),
                          date = unique(date)),
                       on = c("raoult_last","date")]
delay_day[is.na(N),N := 0]
setkey(delay_day,date)
delay_day[,tot := cumsum(N),by = raoult_last]
delay_day[,pc := round(tot/sum(tot)*100,2),by = date]

pc_plot <- ggplot(delay_day,aes(date,pc,fill = raoult_last))+
  geom_area(alpha = .7)+
  scale_x_date(breaks = "year",date_labels = "%Y",
               expand = c(0,0))+
  scale_fill_manual(breaks = colors$breaks,
                    values = colors$color)+
  guides(fill = F)+
  theme_minimal()+
  scale_y_continuous(breaks = seq(0,100,25),
                       labels = paste0(seq(0,100,25)," %"))+
  theme(axis.title.y.left = element_text(margin = margin(r = 3,unit = "mm")),
        axis.text = element_text(color = "black"),
        axis.text.y.left = element_text(margin = margin(r = 0,unit = "mm")),
        panel.grid.major.x= element_blank(),
        panel.grid.minor.x= element_blank(),
        panel.grid.minor.y = element_blank(),
        panel.grid.major.y = element_line(color = "grey50"),
        plot.subtitle = element_text(hjust = 0.5),
        axis.ticks.x = element_line(),
        axis.ticks.length.x = unit(1,"mm"))+
  labs(x = "",
       y = "",subtitle = "Raoult in % of NMNI papers")

pc_plot

#### waffle plot of NMNI publications ####

wafle <- delays[,.(N = .N),by = .(raoult_last,year = year(published))]

setkey(wafle,raoult_last)

wafle[,raoult_last := factor(raoult_last,level = colors[1:3,breaks] )]

wafle_plot <- ggplot(wafle,aes(fill = raoult_last,values = N))+
  geom_waffle(color = "#5c5c5c", size = .2, n_rows = 10, flip = TRUE) +
  facet_wrap(~year, nrow = 1, strip.position = "bottom") +
  scale_x_discrete() + 
  scale_y_continuous(labels = function(x) x * 10, # make this multiplyer the same as n_rows
                     expand = c(0,0)) +
  scale_fill_manual(breaks = colors$breaks,
                    values = colors$color)+
  coord_equal() +
  labs(y = "",
       title = "NMNI publications per year",
       x = ""
  ) +
  theme_minimal(base_family = "Roboto Condensed") +
  theme(panel.grid = element_blank(), axis.ticks.y = element_line(),
        plot.title = element_text(hjust = 0.5),
        axis.text = element_text(color = "black")
        
        ) +
  guides(fill = F)
wafle_plot

#### raoult background ####

transparent <- function(img) {
  magick::image_fx(img, expression = "0.2*a", channel = "alpha")
}

raoult_back <- ggplot()+
  geom_image(aes(x = 1,y = 1,image =  "Didier-raoult.png"),
             image_fun = transparent,
             size = 1,
             asp = 1)+
  theme_void()

##### final layout ####

layout <- c(
  area(t = 1, l = 1, b = 5, r = 3),
  area(t = 1, l = 1, b = 3, r = 3),
  area(t = 1, l = 4, b = 3, r = 6),
  area(t = 4, l = 1, b = 5, r = 6)
)

p <- raoult_back + delay_plot+ pc_plot +  wafle_plot  +
  plot_layout(design = layout)+
  plot_annotation(
    title = 'Dider Raoult and New Microbes and New Infections (NMNI)',
    subtitle = "Publications with <b style='color: #0ee1e8'> D. Raoult as the last author</b>,<br> as <b style='color: #0022ff'>an other author</b>, or <b style='color: #d4d4d4'>not in the author list</b>",
    caption = '@denis_mongin\ndata:NMNI',
    theme = theme(plot.title = element_text(hjust = 0.5),
                  plot.caption = element_text(margin = margin(t = -3,unit = "mm")),
                  plot.subtitle = element_markdown(),
                  plot.background = element_rect(fil = "#7d7d7d"))
  ) & theme(text = element_text('mono'))

ggsave("NMNI.png",p,height = 4.5,width = 8,dpi = 600)
  
#### additional plot ####

plouf <- delays[raoult_last != "not_raoult",.(N = .N,Nlast = sum(raoult_last == "last")),by = .(date = published)] %>%
  arrange(date)%>%
  mutate(tot = cumsum(N),
         tot_last = cumsum(Nlast))


dates_tmp <- lapply(c(50,100,150,200),function(x){
  plouf[tot > x,date[1]]  
}) %>% unlist() %>% as_date()
scales <- data.table(date = dates_tmp,tot = c(50,100,150,200))
scales <- rbind(scales,data.table(date = ymd("2021-03-01"),tot = 250))
overlay <- 170
scales[,xmin := date - 2*overlay]
scales[,xmax := date + overlay]


total <- ggplot(plouf)+
  geom_line(aes(date,tot),color = "lightblue",size = 3)+
  geom_segment(data = scales,
               aes(x = xmin,xend = xmax,y = tot,yend = tot),
               linetype = "dashed")+
  geom_text(data = scales,aes(x = xmin - 130,y = tot,label = tot))+
  theme_void()+
  scale_x_date(breaks = "year",date_labels = "%Y")+
  theme(axis.text.x.bottom = element_text(margin = margin(t = 1,unit = "mm")),
        axis.ticks.x = element_line(),
        axis.ticks.length.x = unit(1,"mm"))

