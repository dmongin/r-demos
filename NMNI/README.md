# NMNI graph

* The file [get_url_list.R](./get_url_list.R) create the list of all url for all NMNI issues
* The file [scape_data.R](./scape_data.R) scrape the data from the url list (authors, dates, paper title) and create a sqlit database
* The file [graph.R](./graph.R) uses the sqlite data to create the graph

![](./NMNI.png)

