# ENSO status and monthly temperatures

This is a complementary analysis based on a graph from Zeke Hausfather:
https://bsky.app/profile/hausfath.bsky.social/post/3lhckwzswq42a

The idea is to plot the distribution of the monthly world mean 2m temperature variation around their time trend, as a function of the ENSO status (la niña/el niño), to see if the shift observed in january 2025 is that far from what usually occurs.

I used ENSO data here: https://origin.cpc.ncep.noaa.gov/products/analysis_monitoring/ensostuff/ONI_v5.php
and temperature data form here: https://climate.copernicus.eu/copernicus-january-2025-was-warmest-record-globally-despite-emerging-la-nina

The code to get the data from the noaa page and to do the graphs is in [get_data.R](get_data.R).

![distribution](graph.png)

![ENSO in time](ENSO_time.png)

![temperature all months](allmonths.png)

![distribution all months](allmonths_distrib.png)


